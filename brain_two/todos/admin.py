from django.contrib import admin
from todos.models import TodoList, ToDoItem
# Register your models here.


class TodoListAdmin(admin.ModelAdmin):
    pass


class TodoItemAdmin(admin.ModelAdmin):
    pass


admin.site.register(TodoList, TodoListAdmin)
admin.site.register(ToDoItem, TodoItemAdmin)
